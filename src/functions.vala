using Cairo;
using Math;


void pre_render(ImageSurface surface, Context ctx) {
    ctx.set_source_rgb(0.0, 0.0, 0.0);
    ctx.rectangle(0, 0, surface.get_width(), surface.get_height());
    ctx.fill();
}

Vector equation(Vector x) {
    return new Vector(
        Math.sin(x.x + 0.5 * x.y) + 1.25 * Math.cos(x.x + x.y - 1.0) - Math.sin(3.5 * Math.sin(0.25*x.y*x.x)) - 1,
        -1.5 * Math.cos(x.x * 0.5 * x.y) + 2 + Math.sin(3.5 * Math.sin(2*x.x))
    );
}

Color color_function(Vector[] path) {
    var trajectory_length = path_length(path);

    var colorramp = new ColorRamp();
    colorramp.add(new ColorRampEntry(
        new Color.from_rgb(0.21176470588235294, 0.9254901960784314, 0.9803921568627451), 3));
    colorramp.add(new ColorRampEntry(
        new Color.from_rgb(0.21176470588235294, 0.9803921568627451, 0.8431372549019608), 6));
            colorramp.add(new ColorRampEntry(
        new Color.from_rgb(0.1450980392156863, 0.6745098039215687, 1.0), 7));
    colorramp.add(new ColorRampEntry(
        new Color.from_rgb(0.1450980392156863, 0.7372549019607844, 0.9921568627450981), 8.5));
    colorramp.add(new ColorRampEntry(
        new Color.from_rgb(0.4745098039215686, 0.9215686274509803, 1.0), 10));
    colorramp.add(new ColorRampEntry(
        new Color.from_rgb(0.4745098039215686, 0.9215686274509803, 1.0), 12));
    colorramp.add(new ColorRampEntry(
        new Color.from_rgb(0.7843137254901961, 1.0, 0.9098039215686274), 14));

    colorramp.scale(0.4);

    colorramp.transform_colors(
        (c) => {
            // Hue shift transform.
            double shift = 90;

            var hc = c.to_hsva();
            hc.h = Math.fmod(hc.h + shift, 360);
            return hc.to_rgba();
        }
    );

    const double variation = 0.25;

    return colorramp.calculate(
        trajectory_length * (1.0 + Random.double_range(-variation, variation)));
}

double path_length(Vector[] path) {
    double length = 0;
    for (int i = 1; i < path.length; i++) {
        length += path[i].copy().sub(path[i-1]).norm2();
    }
    return length;
}

