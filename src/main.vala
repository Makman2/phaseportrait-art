using Math;

delegate Vector Equation(Vector x);

Vector[] simulate(Equation eq, Vector start, int steps=100, double dt=0.01) {
    var trajectory = new Vector[steps+1];
    trajectory[0] = start.copy();
    var prev = start.copy();
    for (int i = 1; i < trajectory.length; i++) {
        var next = eq(prev);
        next.multiply(dt);
        prev.add(next);
        trajectory[i] = prev.copy();
    }
    return trajectory;
}

Vector remap_vector(Domain source_domain, Domain target_domain, Vector pt) {
    return new Vector(
        (pt.x - source_domain.start.x) / source_domain.width * target_domain.width + target_domain.start.x,
        (pt.y - source_domain.start.y) / source_domain.height * target_domain.height + target_domain.start.y
    );
}

public class Color {
    public double r;
    public double g;
    public double b;
    public double a;

    public Color() {
        this.from_rgba(0, 0, 0, 1);
    }

    public Color.from_rgb(double r, double g, double b) {
        this.from_rgba(r, g, b, 1);
    }

    public Color.from_rgba(double r, double g, double b, double a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    public HueColor to_hsva() {
        HueColor huecolor = new HueColor();
        huecolor.a = this.a;

        if (this.r > this.g && this.r > this.b) {
            var delta = this.r - double.min(this.g, this.b);
            huecolor.h = delta == 0.0 ? 0.0 : 60 * Math.fmod((this.g - this.b) / delta, 6);
            huecolor.s = this.r == 0.0 ? 0.0 : delta / this.r;
            huecolor.v = this.r;
        }
        else if (this.g > this.r && this.g > this.b) {
            var delta = this.g - double.min(this.b, this.r);
            huecolor.h = delta == 0.0 ? 0.0 : 60 * ((this.b - this.r) / delta + 2);
            huecolor.s = this.g == 0.0 ? 0.0 : delta / this.g;
            huecolor.v = this.g;
        }
        else {
            var delta = this.b - double.min(this.r, this.g);
            huecolor.h = delta == 0.0 ? 0.0 : 60 * ((this.r - this.g) / delta + 4);
            huecolor.s = this.b == 0.0 ? 0.0 : delta / this.b;
            huecolor.v = this.b;
        }

        return huecolor;
    }
}

public class HueColor {
    public double h;
    public double s;
    public double v;
    public double a;

    public HueColor() {
        this.from_hsva(0, 0, 0, 1);
    }

    public HueColor.from_hsv(double h, double s, double v) {
        this.from_hsva(h, s, v, 1);
    }

    public HueColor.from_hsva(double h, double s, double v, double a) {
        this.h = h;
        this.s = s;
        this.v = v;
        this.a = a;
    }

    public Color to_rgba() {
        var color = new Color();
        color.a = this.a;

        var c = this.v * this.s;
        var x = c * (1 - Math.fabs(Math.fmod(this.h / 60, 2) - 1));
        var m = this.v - c;

        if (h < 60) {
            color.r = c;
            color.g = x;
            color.b = 0;
        }
        else if (h < 120) {
            color.r = x;
            color.g = c;
            color.b = 0;
        }
        else if (h < 180) {
            color.r = 0;
            color.g = c;
            color.b = x;
        }
        else if (h < 240) {
            color.r = 0;
            color.g = x;
            color.b = c;
        }
        else if (h < 300) {
            color.r = x;
            color.g = 0;
            color.b = c;
        }
        else {
            color.r = c;
            color.g = 0;
            color.b = x;
        }

        color.r += m;
        color.g += m;
        color.b += m;

        return color;
    }
}

class ColorRampEntry {
    public Color color;
    public double position;

    public ColorRampEntry(Color color, double position) {
        this.color = color;
        this.position = position;
    }
}

class ColorRamp {
    private List<ColorRampEntry> _colors;

    public ColorRamp() {
        this._colors = new List<ColorRampEntry>();
    }

    public void add(ColorRampEntry entry) {
        this._colors.insert_sorted(
            entry,
            (a, b) => {
                return (int)(a.position > b.position) - (int)(a.position < b.position);
            }
        );
    }

    public Color calculate(double position) {
        if (this._colors.length() == 0) {
            return new Color.from_rgb(0, 0, 0);
        }

        var first = this._colors.first().data;
        if (position < first.position) {
            return first.color;
        }

        var last = this._colors.last().data;
        if (position > last.position) {
            return last.color;
        }

        uint i = 0;
        foreach (var entry in this._colors) {
            if (position < entry.position) {
                break;
            }
            i++;
        }
        unowned List<ColorRampEntry> e = this._colors.nth(i);
        var entry1 = e.prev.data;
        var entry2 = e.data;
        var blendfactor = (position - entry1.position) / (entry2.position - entry1.position);

        return new Color.from_rgba(
            blendfactor * (entry2.color.r - entry1.color.r) + entry1.color.r,
            blendfactor * (entry2.color.g - entry1.color.g) + entry1.color.g,
            blendfactor * (entry2.color.b - entry1.color.b) + entry1.color.b,
            blendfactor * (entry2.color.a - entry1.color.a) + entry1.color.a
        );
    }

    public void scale(double scale) {
        foreach (var entry in this._colors) {
            entry.position *= scale;
        }
    }

    public delegate Color ColorTransformFunc(Color color);
    public void transform_colors(ColorTransformFunc transform) {
        foreach (var entry in this._colors) {
            entry.color = transform(entry.color);
        }
    }
}

[Compact]
public class Vector {
    public double x;
    public double y;

    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector copy() {
        return new Vector(this.x, this.y);
    }

    public unowned Vector add(Vector pt) {
        this.x += pt.x;
        this.y += pt.y;
        return this;
    }

    public unowned Vector sub(Vector pt) {
        this.x -= pt.x;
        this.y -= pt.y;
        return this;
    }

    public unowned Vector multiply(double scalar) {
        this.x *= scalar;
        this.y *= scalar;
        return this;
    }

    public double norm2() {
        return Math.sqrt(this.x*this.x + this.y*this.y);
    }
}


[Compact]
public class Domain {
    public Vector start;
    public Vector end;

    public Domain(Vector start, Vector end) {
        this.start = start.copy();
        this.end = end.copy();
    }

    public Domain.from_values(double x1, double y1, double x2, double y2) {
        this.start = new Vector(x1, y1);
        this.end = new Vector(x2, y2);
    }

    public double width {
        get {
            return this.end.x - this.start.x;
        }
    }

    public double height {
        get {
            return this.end.y - this.start.y;
        }
    }
}


[Compact]
public class ImageSize {
    public int width;
    public int height;

    public ImageSize(int width, int height) {
        this.width = width;
        this.height = height;
    }
}


public class Main : Object {
    private static Domain domain;
    private static ImageSize imagesize;
    private static int trajectory_count;
    private static int trajectory_simulations;
    private static double linewidth;
    private static string output;

    private static void initialize() {
        // Due to limitations when static values are initialized, we have to
        // do it ourselves. Sigh... thank you Vala not taking care of this.
        domain = new Domain.from_values(-10, -10, 10, 10);
        imagesize = new ImageSize(1920, 1080);
        trajectory_count = 10000;
        trajectory_simulations = 500;
        linewidth = 1.0;
        output = "out.png";
    }

    private static bool parse_domain(string option_name, string val, void* data) throws OptionError {
        try {
            var floatrgx = "-?\\d+(?:\\.\\d+)?";
            var regex = new Regex(@"($floatrgx),($floatrgx):($floatrgx),($floatrgx)");
            MatchInfo info;
            if (regex.match_full(val, -1, 0, 0, out info)) {
                domain = new Domain.from_values(
                    double.parse(info.fetch(1)),
                    double.parse(info.fetch(2)),
                    double.parse(info.fetch(3)),
                    double.parse(info.fetch(4)));
            } else {
                throw new OptionError.BAD_VALUE(@"Unrecognized domain specification: $val");
            }
        } catch (RegexError ex) {
            throw new OptionError.FAILED("Regular expression parsing failed.");
        }

        return true;
    }

    private static bool parse_imagesize(string option_name, string val, void* data) throws OptionError {
        switch (val) {
            case "hd":
                imagesize = new ImageSize(1280, 720);
                break;
            case "1k":
                imagesize = new ImageSize(1920, 1080);
                break;
            case "4k":
                imagesize = new ImageSize(3840, 2160);
                break;
            case "16k":
                imagesize = new ImageSize(7680, 4320);
                break;
            case "32k":
                imagesize = new ImageSize(15360, 8640);
                break;
            case "64k":
                imagesize = new ImageSize(30720, 17280);
                break;
            case "128k":
                imagesize = new ImageSize(61440, 34560);
                break;
            default:
                try {
                    var regex = new Regex("(\\d+)x(\\d+)");
                    MatchInfo info;
                    if (regex.match_full(val, -1, 0, 0, out info)) {
                        imagesize = new ImageSize(int.parse(info.fetch(1)), int.parse(info.fetch(2)));
                    } else {
                        throw new OptionError.BAD_VALUE(@"Unrecognized image size specification: $val");
                    }
                } catch (RegexError ex) {
                    throw new OptionError.FAILED("Regular expression parsing failed.");
                }
                break;
        }

        return true;
    }

    private const GLib.OptionEntry[] options = {
        //
        { "domain", 'd', 0, OptionArg.CALLBACK, (void*)parse_domain,
          "Domain of function to render trajectories from. By default -10,-10:10,10.",
          "<X1>,<Y1>:<X2>,<Y2>"},
        { "size", 's', 0, OptionArg.CALLBACK, (void*)parse_imagesize,
          "The output image size. By default 1920x1080 (1k). You can supply also shortcuts like 'hd' or '1k', '4k', ... up to '128k'.",
          "<WIDTH>x<HEIGHT> | hd | <NUM>k" },
        { "count", 'p', 0, OptionArg.INT, ref trajectory_count,
          "The trajectory count. By default 10000.",
          null },
        { "simulations", 'l', 0, OptionArg.INT, ref trajectory_simulations,
          "The number of steps to calculate for a single trajectory. By default 500.",
          null },
        { "linewidth", 'w', 0, OptionArg.DOUBLE, ref linewidth,
          "The line width. By default 1.",
          null },
        { "output", 'o', 0, OptionArg.FILENAME, ref output,
          "Output PNG filename. By default out.png.",
          null },

        // list terminator
        { null }
    };

    public static int main(string[] args) {
        // Due to limitations when static values are initialized, we have to
        // do it ourselves. Sigh... thank you Vala not taking care of this.
        initialize();

        try {
            var opt_context = new OptionContext("phaseportrait-art");
            opt_context.set_help_enabled(true);
            opt_context.add_main_entries(options, null);
            opt_context.parse(ref args);
        } catch (OptionError e) {
            print("error: %s\n", e.message);
            print("Run '%s --help' to see a full list of available command line options.\n", args[0]);
            return 1;
        }

        print(@"Image size: $(imagesize.width)x$(imagesize.height)\n");
        print(@"Domain: $(domain.start.x),$(domain.start.y):$(domain.end.x),$(domain.end.y)\n");
        print(@"Trajectory count: $trajectory_count\n");
        print(@"Trajectory simulations: $trajectory_simulations\n");
        print(@"Line width: $linewidth\n");
        print("\n");

        Domain image_domain = new Domain.from_values(0, 0, imagesize.width, imagesize.height);

        Cairo.ImageSurface surface = new Cairo.ImageSurface(Cairo.Format.ARGB32, imagesize.width, imagesize.height);
        Cairo.Context ctx = new Cairo.Context(surface);

        // Background filling.

        ctx.save();
        pre_render(surface, ctx);
        ctx.restore();

        ctx.set_line_width(linewidth);

        for (int i = 0; i < trajectory_count; i++) {
            var start_vector = new Vector(
                Random.double_range(domain.start.x, domain.end.x),
                Random.double_range(domain.start.y, domain.end.y)
            );

            var pt = remap_vector(domain, image_domain, start_vector);
            ctx.move_to(pt.x, pt.y);

            var simulation = simulate(equation, start_vector, trajectory_simulations);

            var color = color_function(simulation);
            ctx.set_source_rgba(color.r, color.g, color.b, color.a);

            foreach (unowned Vector vec in simulation) {
                pt = remap_vector(domain, image_domain, vec);
                ctx.line_to(pt.x, pt.y);
            }

            ctx.stroke();

            // Progress "bar".
            if (i % (trajectory_count / 100) == 0) {
                print("Rendering: %d%%\r", i / (trajectory_count / 100));
            }
        }
        print("Rendering: 100%%\n");

        print(@"Writing image to $output... ");
        surface.write_to_png(output);
        print("Done.\n");

        return 0;
    }
}
