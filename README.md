# phaseportrait-art

Create beautiful images utilizing phase-portraits of 2D ordinary
differential equations!

## Building

You need Meson, Ninja and Vala:

```bash
meson build && ninja -C build
```

## Running

Run the program:

```bash
build/src/phaseportrait-art
```

It will place an `out.png` in the current working
directory with the final rendered picture!

## Modifying

You can create easily your own phase portraits for different systems
and apply different colors. If you don't intend to do extremely fancy stuff
which requires rewrites, the most interesting things to manipulate are
inside `functions.vala`.

- function `void pre_render(Vector a)`

  Initial rendering step before trajectory rendering. Usually used to setup
  background (e.g. paint a background color).

- function `Vector equation(Vector a)`

  The differential equation for which trajectories are rendered.
  
- function `Color color_function(Vector[] path)`

  The color to use for a trajectory.

You can control additional parameters via program arguments at invocation.
Check `--help` for more information on supported arguments.

## Examples

Here are some cool examples. With different ODE systems and
different amounts of trajectories, colors, etc. You can look them up inside
the `examples/` folder!

![](examples/example1.png)

![](examples/example2.png)

![](examples/example5.png)

![](examples/example6.png)

![](examples/example7.png)

![](examples/example8.png)
